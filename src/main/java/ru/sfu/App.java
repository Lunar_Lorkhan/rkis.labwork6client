package ru.sfu;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.web.client.RestTemplate;
import ru.sfu.model.Perfumery;

/**
 * Матеров Иван
 * Лабораторная работа 6
 */
public class App {
    public static void main(String[] args) throws JsonProcessingException {
        String url = "http://localhost:8081/perfumery/";

        String ret = retrievePerfumery(url, 4L);
        System.out.println(ret.toString());

        String retAll = retrievePerfumerys(url);
        System.out.println(retAll.toString());

        postPerfumery(url, new Perfumery(3.14, 14000, "One", "Two", "Tree"));

        deletePerfumery(url, 4L);

        updatePerfumery(
                url,
                new Perfumery(null, null, null, null, "OneTwoTree"),
                10L);
    }

    public static String retrievePerfumery(String url, Long id) {
        return new RestTemplate().getForObject(
                url + "{id}/",
                String.class,
                id
        );
    }

    public static String retrievePerfumerys(String url) {
        return new RestTemplate().getForObject(
                url,
                String.class
        );
    }

    public static void postPerfumery(String url, Perfumery perfumery) throws JsonProcessingException {
        new RestTemplate().postForObject(url,
                                         perfumery,
                                         Perfumery.class);
    }

    public static void deletePerfumery(String url, Long id) {
        new RestTemplate().delete(url + "{id}/", id);
    }

    public static void updatePerfumery(String url, Perfumery perfumery, Long id) {
//        Почему?!!??!?!?!?!?!??!
        new RestTemplate().put(url + "{id}/",
                               perfumery,
                               id);
    }
}
